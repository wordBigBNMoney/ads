package io.finer.ads.jeecg.service.impl;

import io.finer.ads.jeecg.entity.ShowLogHistory;
import io.finer.ads.jeecg.mapper.ShowLogHistoryMapper;
import io.finer.ads.jeecg.service.IShowLogHistoryService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 呈现日志-历史
 * @Author: jeecg-boot
 * @Date:   2020-06-16
 * @Version: V1.0
 */
@Service
public class ShowLogHistoryServiceImpl extends ServiceImpl<ShowLogHistoryMapper, ShowLogHistory> implements IShowLogHistoryService {

}
